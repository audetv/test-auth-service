# Test Auth Server
Auth Server application


## provisioning
You will need to copy hosts.yml.dist to the hosts.yml file, and you will change the necessary params:

```
ansible_user: root - your user
ansible_host: 0.0.0.0 - your server ip
ansible_port: 22 - your server port
```
cd provisioning, install ansible if necessary:

```sudo apt install ansible```

show hosts

```ansible all -m ping -i hosts.yml```

```audetv@HOL0136:~/projects/test-auth-service/provisioning$ ansible all -m ping -i hosts.yml
Enter passphrase for key '/home/audetv/.ssh/id_rsa':
[WARNING]: Platform linux on host server is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.

server | SUCCESS => {
"ansible_facts": {
"discovered_interpreter_python": "/usr/bin/python"
},
"changed": false,
"ping": "pong"
}
```

```REGISTRY=registry.gitlab.com/audetv/test-auth-service IMAGE_TAG=master-4 make build```

```REGISTRY=registry.gitlab.com/audetv/test-auth-service IMAGE_TAG=master-4 make push```

```HOST=deploy@5.101.50.118 PORT=22 REGISTRY=registry.gitlab.com/audetv/test-auth-service IMAGE_TAG=master-4 BUILD_NUMBER=4 make deploy```

### api-php-cli

```docker compose run --rm api-php-cli php -v```

COMPOSER_ALLOW_SUPERUSER#

If set to 1, this env disables the warning about running commands as root/super user. It also disables automatic clearing of sudo sessions, so you should really only set this if you use Composer as super user at all times like in docker containers.

```docker-compose run --rm api-php-cli composer app orm:schema-tool:drop -- --force``` drop db

```sudo chown $USER:$USER api/src -R```
